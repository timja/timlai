
<?php
	echo "test";
	phpinfo();
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Tim Lai</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="https://cdn.rawgit.com/google/material-design-icons/a6145e16/action/drawable-xxxhdpi/ic_account_circle_black_48dp.png">
		<meta name="theme-color" content="#000">

		<script>

			if (location.protocol != 'https:') {
				location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
			}
			
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-96562127-1', 'auto');
			ga('send', 'pageview');

		</script>	

		<style type="text/css">
			body {
				margin: 0;
				padding: 0;
				display: flex;
				min-height: 100vh;
				flex-direction: column;			
				font-family: "Roboto Thin", "Calibri Light", "Helvetica", "Calibri", sans-serif;
				font-size: 1.5rem;

			}

			ul {
				list-style: square;

			}

			main {
				flex: 1 0 auto;
				padding-bottom: 5rem;
			}

			header {
				width: 100%;
				background-color: #585858;
				color: #fff;
				-webkit-box-shadow: 2px 0px 16px 0px rgba(0,0,0,0.75);
				-moz-box-shadow: 2px 0px 16px 0px rgba(0,0,0,0.75);
				box-shadow: 2px 0px 16px 0px rgba(0,0,0,0.75);
			}

			table {
				text-align: center;
				padding: 0;
				margin: 0;
				font-size: 3rem;
			}

			main p {
				margin-top:  0;
			}

			td {

				background-color: #424242
			}

			a {
				color: #000;
				text-decoration: none;
			}

			h1, h2, h3, h4 {
				  position:relative;
				  margin-bottom: 0;

			}

			h2 a:after, h3 a:after {
				content: url(https://cdn.rawgit.com/google/material-design-icons/master/action/drawable-xxxhdpi/ic_open_in_new_black_12dp.png) ;
				position: absolute;

			}

			h2 a:after {
				top: -0.2em;
			}

			h3 a:after {
				top: -0.5em;
			}

			.container {
				width: 850px;
				margin: auto;
			}

			.tiles a {
				display: block;
				color: #fff;
				text-align: center;
				vertical-align: middle;				
				text-decoration: none;
				transition: 0.2s;
				height: 400px;
				width: 400px;
				line-height: 400px;
			}

			.black:hover {
				background-color: #000;
			}

			.blue:hover {
				background-color: #2196f3;
			}			
			.kalender:hover {
				background-color: #fff7d7;
				color: black;
			}	

			.green:hover {
				background-color: green;
			}		

			.s {
				font-size: 1rem;
			}

			footer {
				position: fixed;
				bottom: 0;
				width: 100%;
				background-color: #000000;
				color: #ffffff;
			}


			@media screen and (max-width: 850px) {
				.container {
					width: 95%;
					margin: auto;
				}

				.tiles a {
					height: 45vmin;
					width: 45vmin;
					line-height: 45vmin;
				}

				table {
					font-size: 2rem;					
					margin: auto;
				}
			}
		</style>
	</head>
	
	<body>

		<header>
			<div class="container">
				<h1>Tim Lai - Bewerberwebsite</h1>
			</div>
		</header>
		<main>
			<div class="container">

				<h1>Projekte</h1>

				<table class="tiles"> 
					<tbody>
						<tr>
							<td><a href="http://notiz.cf/timlai#md" class="black" target="_blank">notiz.cf</a></td>
							<td><a href="http://lern.cf/" class="blue" target="_blank">Lern.cf</a></td>
						</tr>
						<tr>
							<td><a href="https://pgp-timlai1.rhcloud.com" class="green" target="_blank">PGPtool</a></td>
							<td><a href="https://sites.google.com/view/ft18c" class="kalender" target="_blank">Schulkalender</a></td>
						</tr>					
					</tbody>
				</table>



				<h1>Schulabschluss</h1>
				<h2><a href="http://www.gymnasium-uetze.de/" target="_blank">Gymnasium Unter den Eichen Uetze</a></h2>
				<p>2010-2015</p>
				<h2><a href="http://www.bbs-burgdorf.de" target="_blank">BBS Burgdorf - Berufliches Gymnasium Technik</a></h2>
				<p>2015-2018</p>

				<p>Angestrebter Abschluss: Allgemeine Hochschulreife</p>
				<h3>Prüfungsfächer mit erhöhten Anforderungen</h3>
				<ul>
					<li>Informationstechnik</li>
					<li>Mathematik</li>
					<li>Englisch</li>
				</ul>

				<h3>Prüfungsfächer mit grundlegender Anforderungen</h3>
				<ul>
					<li>Physik</li>
					<li>Betriebs- und Volkswirtschaft</li>
				</ul>


				<h1>Qualifikationen</h1>

				<ul>
					<li>Microsoft Office
						<ul>
							<li>Powerpoint</li>
							<li>Word</li>
							<li>Excel</li>
							<li>Publisher</li>
						</ul>
					</li>

					<li>Content Management Systeme (Wordpress)</li>

				</ul>
				<h2>Scriptung und Programmierung</h2>
				<ul>
					<li>HTML</li>
					<li>PHP</li>
					<li>JavaScript</li>
					<li>Visual Basic for Applications (VBA)</li>
				</ul>

				<h1>Berufserfahrung</h1>
				<h2>Schülerpraktika</h2>

				
				<h3><a href="https://www.bundesbank.de/Redaktion/DE/Standardartikel/Bundesbank/Karriere/praktika_schueler.html" target="_blank">Deutsche Bundesbank</a></h3>
				
				<h4>Praktikum im Technischen Bereich</h4>

				<p>Zwei Wochen</p>
				<ul>
					<li>Organisation der Anwendungsentwicklung in der Bundesbank</li>
					<li>Werkzeuge und Datenbanken</li>
					<li>Anwendungstests</li>
					<li>Dokumentation</li>
					<li>Tätigkeiten eines Anwendungsentwicklers (JAVA)</li>
				</ul>
				<br>

				<h3>				
					<a href="https://www.siemens.de/jobs/schulabsolventen/schuelerpraktikum/seiten/home.aspx" target="_blank">Siemens AG</a>
				</h3>

				<h4>Human Resources People and Leadership</h4>

				<p>Eine Woche</p>

				<ul>
					<li>Eigenverantwortliches Arbeiten</li>
					<li>Elektrotechnische Abläufe und Prozesse</li>
					<li>Einblicke zur technischen Berufsausbildung</li>
				</ul>


				<h1>Schultätigkeiten</h1>
				<ul>
					<li>Ehemaliger Administrator der Schul-IT in Uetze</li>
					<li>Klassensprecher (mehrmals)</li>
					<li>Mitglied der Schülervertretung</li>
					<li>Organistor vom Abiturienten Ball</li>
				</ul>

				<h1>Freizeitbeschäftigung</h1>

				<ul>
					<li>Programmierung</li>
					<li>Einrichten von Webseiten für Firmen und Vereine</li>
					<li>Verfolgung von Kryptowährungen</li>
					<li>Lesen von Büchern</li>
					<li>Musizieren im Verein</li>
				</ul>


			<hr>

			 <a class="s" href="https://tools.google.com/dlpage/gaoptout?hl=de">Diese Seite benutzt Google Analytics, welches Sie hier deaktivieren können</a>


			</div>


		</main>

		<footer>
			<div class="container">
				Copyright Tim Lai 2017
			</div>
		</footer>
	<!--</body>

</html>